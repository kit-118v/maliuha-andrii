using Microsoft.VisualStudio.TestTools.UnitTesting;
using ConsoleApp2;
namespace ConsoleApp2.Tests
{
    [TestClass]
    public class MyCollectionTest
    {
        [TestMethod]
        public void Count_LengthOfArray_ReturnSameNumber()
        {
            var myCollection = new MyCollection<Student>();
            Assert.AreEqual(0, myCollection.Count(), "Count method isn't valid");
        }

        [TestMethod]
        public void Add_IndexAccess_SingleObject_ReturnSameObject()
        {
            var myCollection = new MyCollection<Student>();
            var obj = new Student();
            obj.LastName = "Maliuha";
            obj.Initials = "A. V.";
            obj.Birthday = new System.DateTime(2001, 8, 1);
            obj.EnterDate = new System.DateTime(2018, 9, 1);
            obj.IndexGroup = 'v';
            obj.Faculty = "Programming engineering";
            obj.Specialization = "System programming";
            obj.AverageRating = 83;
            myCollection.add(obj);

            var expectedObj = new Student();
            expectedObj.LastName = "Maliuha";
            expectedObj.Initials = "A. V.";
            expectedObj.Birthday = new System.DateTime(2001, 8, 1);
            expectedObj.EnterDate = new System.DateTime(2018, 9, 1);
            expectedObj.IndexGroup = 'v';
            expectedObj.Faculty = "Programming engineering";
            expectedObj.Specialization = "System programming";
            expectedObj.AverageRating = 83;
            Assert.AreEqual(expectedObj.LastName, myCollection[0].LastName, "LastName isn't valid");
            Assert.AreEqual(expectedObj.Initials, myCollection[0].Initials, "Initials isn't valid");
            Assert.AreEqual(expectedObj.Birthday, myCollection[0].Birthday, "Birthday isn't valid");
            Assert.AreEqual(expectedObj.EnterDate, myCollection[0].EnterDate, "EnterDate isn't valid");
            Assert.AreEqual(expectedObj.IndexGroup, myCollection[0].IndexGroup, "IndexGroup isn't valid");
            Assert.AreEqual(expectedObj.Faculty, myCollection[0].Faculty, "Faculty isn't valid");
            Assert.AreEqual(expectedObj.Specialization, myCollection[0].Specialization, "Specialization isn't valid");
            Assert.AreEqual(expectedObj.AverageRating, myCollection[0].AverageRating, "AverageRating isn't valid");
        }

        [TestMethod]
        public void GetEnumerator_Foreach_ReturnSameCollection()
        {
            var myCollection = new MyCollection<Student>();
            var recieved = new MyCollection<Student>();
            var obj1 = new Student();
            obj1.LastName = "Maliuha";
            obj1.Initials = "A. V.";
            obj1.Birthday = new System.DateTime(2001, 8, 1);
            obj1.EnterDate = new System.DateTime(2018, 9, 1);
            obj1.IndexGroup = 'v';
            obj1.Faculty = "Programming engineering";
            obj1.Specialization = "System programming";
            obj1.AverageRating = 83;
            myCollection.add(obj1);

            var obj2 = new Student();
            obj2.LastName = "Winchester";
            obj2.Initials = "V. G.";
            obj2.Birthday = new System.DateTime(1971, 12, 15);
            obj2.EnterDate = new System.DateTime(2016, 9, 1);
            obj2.IndexGroup = 'a';
            obj2.Faculty = "Programming engineering";
            obj2.Specialization = "System programming";
            obj2.AverageRating = 90;
            myCollection.add(obj2);

            foreach (var obj in myCollection) {
                recieved.add(obj);
            }

            var i = 0;
            foreach (var obj in myCollection)
            {
                Assert.AreEqual(obj.LastName, recieved[i].LastName, "LastName isn't valid");
                Assert.AreEqual(obj.Initials, recieved[i].Initials, "Initials isn't valid");
                Assert.AreEqual(obj.Birthday, recieved[i].Birthday, "Birthday isn't valid");
                Assert.AreEqual(obj.EnterDate, recieved[i].EnterDate, "EnterDate isn't valid");
                Assert.AreEqual(obj.IndexGroup, recieved[i].IndexGroup, "IndexGroup isn't valid");
                Assert.AreEqual(obj.Faculty, recieved[i].Faculty, "Faculty isn't valid");
                Assert.AreEqual(obj.Specialization, recieved[i].Specialization, "Specialization isn't valid");
                Assert.AreEqual(obj.AverageRating, recieved[i].AverageRating, "AverageRating isn't valid");
                i++;
            }
        }
    }
}

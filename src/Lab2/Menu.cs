﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace ConsoleApp2
{
    class Menu
    {
        MyCollection<Student> students;

        public void MainMenu()
        {
            students = new MyCollection<Student>();
            string selection;
            var number = 0;

            while (number != 1)
            {
                Settings();
                selection = Console.ReadLine();

                if (int.TryParse(selection, out number))
                {
                    switch (number)
                    {
                        case 2:
                            Console.Clear();
                            ReadStudent();
                            Console.Clear();
                            break;
                        case 3:
                            Console.Clear();
                            ShowAll();
                            Console.ReadLine();
                            Console.Clear();
                            break;
                        case 4:
                            Console.Clear();
                            PrintByIndex();
                            Console.ReadLine();
                            Console.Clear();
                            break;
                    }
                }
            }
        }

        void Settings()
        {
            Console.WriteLine("Amount of students: " + students.Count());
            Console.WriteLine("1 - exit");
            Console.WriteLine("2 - add new student");
            Console.WriteLine("3 - show all students");
            Console.WriteLine("4 - get student by index");
            Console.Write("Your choice: ");
        }

        void ReadStudent()
        {
            var student = new Student();
            string str;
            char index;
            int num;

            for (int i = 4; i > 0; i--)
            {
                Console.Write("Enter surname of student: ");
                str = Console.ReadLine();
                string patternLastName = @"^[A-Z][a-z]+$";
                if (!Regex.IsMatch(str, patternLastName))
                {
                    Console.WriteLine("Invalid surname. You have " + (i - 1) + " attempts");
                    if (i == 1)
                    {
                        return;
                    }
                }
                else
                {
                    student.LastName = str;
                    i = 0;
                }
            }

            for (int i = 4; i > 0; i--)
            {
                Console.Write("Enter initials: ");
                string patternInitials = @"[A-Z][.][ ][A-Z][.]$";
                str = Console.ReadLine();
                if (!Regex.IsMatch(str, patternInitials))
                {
                    Console.WriteLine("Invalid initials. You have " + (i - 1) + " attempts");
                    if (i == 1)
                    {
                        return;
                    }
                }
                else
                {
                    student.Initials = str;
                    i = 0;
                }
            }

            Console.WriteLine("Enter date of birthday");
            Console.Write("Enter year: ");
            int year = Convert.ToInt32(Console.ReadLine());
            if (year > 2020 && year < 1900)
            {
                Console.WriteLine("Invalid year");
            }
            Console.Write("Enter month: ");
            int month = Convert.ToInt32(Console.ReadLine());
            if (month > 12 && month < 1)
            {
                Console.WriteLine("Invalid month");
            }
            Console.Write("Enter day: ");
            int day = Convert.ToInt32(Console.ReadLine());
            if (day > 31 && day < 1)
            {
                Console.WriteLine("Invalid day");
            }
            student.Birthday = new DateTime(year, month, day);

            Console.WriteLine("Enter date of adding student to list: ");
            Console.Write("Enter year: ");
            year = Convert.ToInt32(Console.ReadLine());
            if (year > 2020 && year < 1900)
            {
                Console.WriteLine("Invalid year");
            }
            Console.Write("Enter month: ");
            month = Convert.ToInt32(Console.ReadLine());
            if (month > 12 && month < 1)
            {
                Console.WriteLine("Invalid month");
            }
            Console.Write("Enter day: ");
            day = Convert.ToInt32(Console.ReadLine());
            if (day > 31 && day < 1)
            {
                Console.WriteLine("Invalid day");
            }
            student.EnterDate = new DateTime(year, month, day);

            for (int i = 4; i > 0; i--)
            {
                Console.Write("Enter index of group: ");
                str = Console.ReadLine();
                if (str.Length > 1)
                {
                    Console.Write("Invalid index of group. You have " + (i - 1) + " attempts");
                    if (i == 1)
                    {
                        return;
                    }
                }
                else
                {
                    index = char.Parse(str);
                    student.IndexGroup = index;
                    i = 0;
                }
            }

            Console.Write("Enter faculty: ");
            str = Console.ReadLine();
            student.Faculty = str;

            Console.Write("Enter specialization: ");
            str = Console.ReadLine();
            student.Specialization = str;

            for (int i = 4; i > 0; i--)
            {
                Console.Write("Enter average rating: ");
                num = Convert.ToInt32(Console.ReadLine());
                if (num > 0 && num <= 100)
                {
                    student.AverageRating = num;
                    i = 0;
                }
                else
                {
                    Console.Write("Invalid rating. You have " + (i - 1) + " attempts");
                    if (i == 1)
                    {
                        return;
                    }
                }
            }
            students.Add(student);
        }

        void ShowAll()
        {
            Console.WriteLine("-----------------------Students-----------------------");
            Console.WriteLine("");
            string str;
            string[] infoStudent;
            var i = 0;
            foreach (var student in students)
            {
                str = student.ToString();
                infoStudent = str.Split(new char[] { '|' });

                Console.WriteLine("Index of student: " + i);
                Console.WriteLine("Name: " + infoStudent[0] + ' ' + infoStudent[1]);
                Console.WriteLine("Birthday: " + infoStudent[2]);
                Console.WriteLine("Enter date: " + infoStudent[3]);
                Console.WriteLine("Inedx of group: " + infoStudent[4]);
                Console.WriteLine("Faculty: " + infoStudent[5]);
                Console.WriteLine("Specialization: " + infoStudent[6]);
                Console.WriteLine("Average rating: " + infoStudent[7]);
                Console.WriteLine("");
                i++;
            }

            Console.WriteLine("------------------------------------------------------");
        }

        void PrintByIndex()
        {
            int index;
            Console.WriteLine("Set index: ");
            index = Convert.ToInt32(Console.ReadLine());
            if (index >= students.Count() || index < 0)
            {
                Console.WriteLine("Invalid index");
                return;
            }
            var str = students[index].ToString();
            var infoStudent = str.Split(new char[] { '|' });

            Console.WriteLine("-----------------------Students-----------------------");
            Console.WriteLine("");
            Console.WriteLine("Index of student: " + index);
            Console.WriteLine("Name: " + infoStudent[0] + ' ' + infoStudent[1]);
            Console.WriteLine("Birthday: " + infoStudent[2]);
            Console.WriteLine("Enter date: " + infoStudent[3]);
            Console.WriteLine("Inedx of group: " + infoStudent[4]);
            Console.WriteLine("Faculty: " + infoStudent[5]);
            Console.WriteLine("Specialization: " + infoStudent[6]);
            Console.WriteLine("Average rating: " + infoStudent[7]);
            Console.WriteLine("");
            Console.WriteLine("------------------------------------------------------");
        }
    }
}

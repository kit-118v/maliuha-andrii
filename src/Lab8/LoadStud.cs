﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1
{
    public class LoadStud
    {
        public static MyCollection<Student> students = ReadFile();

        public static MyCollection<Student> ReadFile()
        {
            MyCollection<Student> temp = new MyCollection<Student>();

            string path = @"C:\Users\user\Desktop\students.txt";
            try
            {
                using (StreamReader sr = new StreamReader(path))
                {
                    string line;
                    while ((line = sr.ReadLine()) != null)
                    {
                        var infoStudent = line.Split('|');
                        var obj = new Student(infoStudent[0], infoStudent[1], DateTime.Parse(infoStudent[2]), DateTime.Parse(infoStudent[3]),
                            char.Parse(infoStudent[4]), infoStudent[5], infoStudent[6], int.Parse(infoStudent[7]));
                        temp.Add(obj);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            return temp;
        }
    }
}

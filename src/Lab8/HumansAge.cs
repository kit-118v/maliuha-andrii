﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1
{
    public class HumansAge
    {
        public int Year { get; set; }
        public int Month { get; set; }
        public int Day { get; set; }

        public HumansAge()
        {

        }

        public override string ToString()
        {
            return Year + " years " + Month + " months " + Day + " days";
        }
    }
}

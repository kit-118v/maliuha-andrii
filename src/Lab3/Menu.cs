﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace ConsoleApp2
{
    class Menu
    {
        MyCollection<Student> students;

        public void MainMenu()
        {
            students = new MyCollection<Student>();
            string selection;
            var number = 0;

            while (number != 1)
            {
                Settings();
                selection = Console.ReadLine();

                if (int.TryParse(selection, out number))
                {
                    switch (number)
                    {
                        case 2:
                            Console.Clear();
                            ReadStudent();
                            Console.Clear();
                            break;
                        case 3:
                            Console.Clear();
                            ShowAll();
                            Console.ReadLine();
                            Console.Clear();
                            break;
                        case 4:
                            Console.Clear();
                            PrintByIndex();
                            Console.ReadLine();
                            Console.Clear();
                            break;
                        case 5:
                            Console.Clear();
                            WriteToFile();
                            break;
                        case 6:
                            Console.Clear();
                            ReadFromFile();
                            break;
                        case 7:
                            Console.Clear();
                            RedactStudent();
                            Console.Clear();
                            break;
                    }
                }
            }
        }

        void Settings()
        {
            Console.WriteLine("Amount of students: " + students.Count());
            Console.WriteLine("1 - exit");
            Console.WriteLine("2 - add new student");
            Console.WriteLine("3 - show all students");
            Console.WriteLine("4 - get student by index");
            Console.WriteLine("5 - save in file");
            Console.WriteLine("6 - read from file");
            Console.WriteLine("7 - redact student");
            Console.Write("Your choice: ");
        }

        void ReadStudent()
        {
            var student = new Student();
            string str;
            char index;
            int num;

            for (int i = 4; i > 0; i--)
            {
                Console.Write("Enter surname of student: ");
                str = Console.ReadLine();
                string patternLastName = @"^[A-Z][a-z]+$";
                if (!Regex.IsMatch(str, patternLastName))
                {
                    Console.WriteLine("Invalid surname. You have " + (i - 1) + " attempts");
                    if (i == 1)
                    {
                        return;
                    }
                }
                else
                {
                    student.LastName = str;
                    i = 0;
                }
            }

            for (int i = 4; i > 0; i--)
            {
                Console.Write("Enter initials: ");
                string patternInitials = @"[A-Z][.][ ][A-Z][.]$";
                str = Console.ReadLine();
                if (!Regex.IsMatch(str, patternInitials))
                {
                    Console.WriteLine("Invalid initials. You have " + (i - 1) + " attempts");
                    if (i == 1)
                    {
                        return;
                    }
                }
                else
                {
                    student.Initials = str;
                    i = 0;
                }
            }

            Console.WriteLine("Enter date of birthday");
            Console.Write("Enter year: ");
            int year = Convert.ToInt32(Console.ReadLine());
            if (year > 2020 && year < 1900)
            {
                Console.WriteLine("Invalid year");
            }
            Console.Write("Enter month: ");
            int month = Convert.ToInt32(Console.ReadLine());
            if (month > 12 && month < 1)
            {
                Console.WriteLine("Invalid month");
            }
            Console.Write("Enter day: ");
            int day = Convert.ToInt32(Console.ReadLine());
            if (day > 31 && day < 1)
            {
                Console.WriteLine("Invalid day");
            }
            student.Birthday = new DateTime(year, month, day);

            Console.WriteLine("Enter date of adding student to list: ");
            Console.Write("Enter year: ");
            year = Convert.ToInt32(Console.ReadLine());
            if (year > 2020 && year < 1900)
            {
                Console.WriteLine("Invalid year");
            }
            Console.Write("Enter month: ");
            month = Convert.ToInt32(Console.ReadLine());
            if (month > 12 && month < 1)
            {
                Console.WriteLine("Invalid month");
            }
            Console.Write("Enter day: ");
            day = Convert.ToInt32(Console.ReadLine());
            if (day > 31 && day < 1)
            {
                Console.WriteLine("Invalid day");
            }
            student.EnterDate = new DateTime(year, month, day);

            for (int i = 4; i > 0; i--)
            {
                Console.Write("Enter index of group: ");
                str = Console.ReadLine();
                if (str.Length > 1)
                {
                    Console.Write("Invalid index of group. You have " + (i - 1) + " attempts");
                    if (i == 1)
                    {
                        return;
                    }
                }
                else
                {
                    index = char.Parse(str);
                    student.IndexGroup = index;
                    i = 0;
                }
            }

            Console.Write("Enter faculty: ");
            str = Console.ReadLine();
            student.Faculty = str;

            Console.Write("Enter specialization: ");
            str = Console.ReadLine();
            student.Specialization = str;

            for (int i = 4; i > 0; i--)
            {
                Console.Write("Enter average rating: ");
                num = Convert.ToInt32(Console.ReadLine());
                if (num > 0 && num <= 100)
                {
                    student.AverageRating = num;
                    i = 0;
                }
                else
                {
                    Console.Write("Invalid rating. You have " + (i - 1) + " attempts");
                    if (i == 1)
                    {
                        return;
                    }
                }
            }
            students.Add(student);
        }

        void ShowAll()
        {
            Console.WriteLine("-----------------------Students-----------------------");
            Console.WriteLine("");
            string str;
            string[] infoStudent;
            var i = 0;
            foreach (var student in students)
            {
                str = student.ToString();
                infoStudent = str.Split(new char[] { '|' });

                Console.WriteLine("Index of student: " + i);
                Console.WriteLine("Name: " + infoStudent[0] + ' ' + infoStudent[1]);
                Console.WriteLine("Birthday: " + infoStudent[2]);
                Console.WriteLine("Enter date: " + infoStudent[3]);
                Console.WriteLine("Inedx of group: " + infoStudent[4]);
                Console.WriteLine("Faculty: " + infoStudent[5]);
                Console.WriteLine("Specialization: " + infoStudent[6]);
                Console.WriteLine("Average rating: " + infoStudent[7]);
                Console.WriteLine("");
                i++;
            }

            Console.WriteLine("------------------------------------------------------");
        }

        void PrintByIndex()
        {
            int index;
            Console.Write("Set index: ");
            index = Convert.ToInt32(Console.ReadLine());
            if (index >= students.Count() || index < 0)
            {
                Console.WriteLine("Invalid index");
                return;
            }
            var str = students[index].ToString();
            var infoStudent = str.Split(new char[] { '|' });

            Console.WriteLine("-----------------------Students-----------------------");
            Console.WriteLine("");
            Console.WriteLine("Index of student: " + index);
            Console.WriteLine("Name: " + infoStudent[0] + ' ' + infoStudent[1]);
            Console.WriteLine("Birthday: " + infoStudent[2]);
            Console.WriteLine("Enter date: " + infoStudent[3]);
            Console.WriteLine("Inedx of group: " + infoStudent[4]);
            Console.WriteLine("Faculty: " + infoStudent[5]);
            Console.WriteLine("Specialization: " + infoStudent[6]);
            Console.WriteLine("Average rating: " + infoStudent[7]);
            Console.WriteLine("");
            Console.WriteLine("------------------------------------------------------");
        }

        public void WriteToFile()
        {
            var path = @"C:\Users\user\Desktop\students.txt";
            try
            {
                using (StreamWriter sw = new StreamWriter(path, false, System.Text.Encoding.Default))
                {
                    foreach (var student in students)
                    {
                        sw.WriteLine(student.ToString());
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public void ReadFromFile()
        {
            var path = @"C:\Users\user\Desktop\students.txt";
            try
            {
                using (StreamReader sr = new StreamReader(path, System.Text.Encoding.Default))
                {
                    string line;
                    while ((line = sr.ReadLine()) != null)
                    {
                        var infoStudent = line.Split(new char[] { '|' });
                        var obj = new Student();
                        obj.LastName = infoStudent[0];
                        obj.Initials = infoStudent[1];
                        obj.Birthday = DateTime.Parse(infoStudent[2]);
                        obj.EnterDate = DateTime.Parse(infoStudent[3]);
                        obj.IndexGroup = char.Parse(infoStudent[4]);
                        obj.Faculty = infoStudent[5];
                        obj.Specialization = infoStudent[6];
                        obj.AverageRating = int.Parse(infoStudent[7]);
                        students.Add(obj);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public void RedactStudent()
        {
            if (students.Count() == 0)
            {
                Console.WriteLine("The list of students is empty");
                Console.ReadLine();
                return;
            }
            string selection;
            var number = 0;
            var status = false;
            int index = 0;
            while (status == false)
            {
                Console.Write("Choose the index of student: ");
                index = Convert.ToInt32(Console.ReadLine());
                if (index < students.Count() && index >= 0)
                {
                    status = true;
                }
            }

            Console.WriteLine("1 - exit");
            Console.WriteLine("to redact: ");
            Console.WriteLine("2 - name");
            Console.WriteLine("3 - initials");
            Console.WriteLine("4 - birthday");
            Console.WriteLine("5 - enter date");
            Console.WriteLine("6 - index of group");
            Console.WriteLine("7 - faculty");
            Console.WriteLine("8 - specialization");
            Console.WriteLine("9 - average rating");
            selection = Console.ReadLine();
            
            if (int.TryParse(selection, out number))
            {
                    
                switch (number)
                {
                    case 1:

                        return;

                    case 2:
                        for (int i = 4; i > 0; i--)
                        {
                            Console.Write("Enter surname of student: ");
                            string str3 = Console.ReadLine();
                            string patternLastName = @"^[A-Z][a-z]+$";
                            if (!Regex.IsMatch(str3, patternLastName))
                            {
                                Console.WriteLine("Invalid surname. You have " + (i - 1) + " attempts");
                                if (i == 1)
                                {
                                    return;
                                }
                            }
                            else
                            {
                                students[index].LastName = str3;
                                i = 0;
                            }
                        }
                        break;

                    case 3:

                        for (int i = 4; i > 0; i--)
                        {
                            Console.Write("Enter initials: ");
                            string patternInitials = @"[A-Z][.][ ][A-Z][.]$";
                            var str2 = Console.ReadLine();
                            if (!Regex.IsMatch(str2, patternInitials))
                            {
                                Console.WriteLine("Invalid initials. You have " + (i - 1) + " attempts");
                                if (i == 1)
                                {
                                    return;
                                }
                            }
                            else
                            {
                                students[index].Initials = str2;
                                i = 0;
                            }
                        }
                        break;

                    case 4:
                        Console.WriteLine("Enter date of birthday");
                        Console.Write("Enter year: ");
                        int year = Convert.ToInt32(Console.ReadLine());
                        if (year > 2020 && year < 1900)
                        {
                            Console.WriteLine("Invalid year");
                        }
                        Console.Write("Enter month: ");
                        int month = Convert.ToInt32(Console.ReadLine());
                        if (month > 12 && month < 1)
                        {
                            Console.WriteLine("Invalid month");
                        }
                        Console.Write("Enter day: ");
                        int day = Convert.ToInt32(Console.ReadLine());
                        if (day > 31 && day < 1)
                        {
                            Console.WriteLine("Invalid day");
                        }
                        students[index].Birthday = new DateTime(year, month, day);
                        break;
                        
                    case 5:

                        Console.WriteLine("Enter date of adding student to list");
                        Console.Write("Enter year: ");
                        year = Convert.ToInt32(Console.ReadLine());
                        if (year > 2020 && year < 1900)
                        {
                            Console.WriteLine("Invalid year");
                        }
                        Console.Write("Enter month: ");
                        month = Convert.ToInt32(Console.ReadLine());
                        if (month > 12 && month < 1)
                        {
                            Console.WriteLine("Invalid month");
                        }
                        Console.Write("Enter day: ");
                        day = Convert.ToInt32(Console.ReadLine());
                        if (day > 31 && day < 1)
                        {
                            Console.WriteLine("Invalid day");
                        }
                        students[index].EnterDate = new DateTime(year, month, day);

                        break;
                        
                    case 6:

                        for (int i = 4; i > 0; i--)
                        {
                            Console.Write("Enter index of group: ");
                            var str1 = Console.ReadLine();
                            if (str1.Length > 1)
                            {
                                Console.Write("Invalid index of group. You have " + (i - 1) + " attempts");
                                if (i == 1)
                                {
                                    return;
                                }
                            }
                            else
                            {
                                var groupIndex = char.Parse(str1);
                                students[index].IndexGroup = groupIndex;
                                i = 0;
                            }
                        }
                        break;

                    case 7:

                        Console.Write("Enter faculty: ");
                        var str4 = Console.ReadLine();
                        students[index].Faculty = str4;

                        

                        break;

                    case 8:

                        Console.Write("Enter specialization: ");
                        var str5 = Console.ReadLine();
                        students[index].Specialization = str5;

                        break;

                    case 9:

                        for (int i = 4; i > 0; i--)
                        {
                            Console.Write("Enter average rating: ");
                            var num = Convert.ToInt32(Console.ReadLine());
                            if (num > 0 && num <= 100)
                            {
                                students[index].AverageRating = num;
                                i = 0;
                            }
                            else
                            {
                                Console.Write("Invalid rating. You have " + (i - 1) + " attempts");
                                if (i == 1)
                                {
                                    return;
                                }
                            }
                        }

                        break;
                }

                Console.WriteLine("The result of redaction: ");
                var str = students[index].ToString();
                var infoStudent = str.Split(new char[] { '|' });

                Console.WriteLine("-----------------------Students-----------------------");
                Console.WriteLine("");
                Console.WriteLine("Index of student: " + index);
                Console.WriteLine("Name: " + infoStudent[0] + ' ' + infoStudent[1]);
                Console.WriteLine("Birthday: " + infoStudent[2]);
                Console.WriteLine("Enter date: " + infoStudent[3]);
                Console.WriteLine("Inedx of group: " + infoStudent[4]);
                Console.WriteLine("Faculty: " + infoStudent[5]);
                Console.WriteLine("Specialization: " + infoStudent[6]);
                Console.WriteLine("Average rating: " + infoStudent[7]);
                Console.WriteLine("");
                Console.WriteLine("------------------------------------------------------");

            }
        }

    }
}

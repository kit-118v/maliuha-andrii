﻿using System;
using System.ComponentModel;
using System.Text.RegularExpressions;

namespace FirstApplication
{
	public class Menu
	{
        Students students;

        public void MainMenu()
		{
            students = new Students();
            students.createArr(0);
            string selection;
            int number = 0;

            while (number != 1)
            {
                settings();
                selection = Console.ReadLine();

                if (int.TryParse(selection, out number))
                {
                    switch (number)
                    {
                        case 2:
                            Console.Clear();
                            readStudent();
                            Console.Clear();
                            break;
                        case 3:
                            Console.Clear();
                            showAll();
                            Console.ReadLine();
                            Console.Clear();
                            break;
                    }
                }
            }
        }

        void settings()
        {
            Console.WriteLine("Amount of students: " + students.getLength());
            Console.WriteLine("1 - exit");
            Console.WriteLine("2 - add new student");
            Console.WriteLine("3 - show all students");
            Console.Write("Your choice: ");
        }

        void readStudent()
        {
            var student = new Student();
            string str;
            char index;
            int num;

            for (int i = 4; i > 0; i--)
            {
                Console.Write("Enter surname of student: ");
                str = Console.ReadLine();
                string patternLastName = @"^[A-Z][a-z]+$";
                if (!Regex.IsMatch(str, patternLastName))
                {
                    Console.WriteLine("Invalid surname. You have " + (i - 1) + " attempts");
                    if (i == 1)
                    {
                        return;
                    }
                }
                else
                {
                    student.LastName = str;
                    i = 0;
                }
            }

            for (int i = 4; i > 0; i--)
            {
                Console.Write("Enter initials: ");
                string patternInitials = @"[A-Z][.][ ][A-Z][.]$";
                str = Console.ReadLine();
                if (!Regex.IsMatch(str, patternInitials))
                {
                    Console.WriteLine("Invalid initials. You have " + (i - 1) + " attempts");
                    if (i == 1)
                    {
                        return;
                    }
                }
                else
                {
                    student.Initials = str;
                    i = 0;
                }
            }

            Console.WriteLine("Enter date of birthday");
            Console.Write("Enter year: ");
            int year = Convert.ToInt32(Console.ReadLine());
            if (year > 2020 && year < 1900)
            {
                Console.WriteLine("Invalid year");
            }
            Console.Write("Enter month: ");
            int month = Convert.ToInt32(Console.ReadLine());
            if (month > 12 && month < 1)
            {
                Console.WriteLine("Invalid month");
            }
            Console.Write("Enter day: ");
            int day = Convert.ToInt32(Console.ReadLine());
            if (day > 31 && day < 1)
            {
                Console.WriteLine("Invalid day");
            }
            student.Birthday = new DateTime(year, month, day);

            Console.WriteLine("Enter date of adding student to list: ");
            year = Convert.ToInt32(Console.ReadLine());
            if (year > 2020 && year < 1900)
            {
                Console.WriteLine("Invalid year");
            }
            Console.Write("Enter month: ");
            month = Convert.ToInt32(Console.ReadLine());
            if (month > 12 && month < 1)
            {
                Console.WriteLine("Invalid month");
            }
            Console.Write("Enter day: ");
            day = Convert.ToInt32(Console.ReadLine());
            if (day > 31 && day < 1)
            {
                Console.WriteLine("Invalid day");
            }
            student.EnterDate = new DateTime(year, month, day);

            for (int i = 4; i > 0; i--)
            {
                Console.Write("Enter index of group: ");
                str = Console.ReadLine();
                if (str.Length > 1)
                {
                    Console.Write("Invalid index of group. You have " + (i - 1) + " attempts");
                    if (i == 1)
                    {
                        return;
                    }
                }
                else
                {
                    index = char.Parse(str);
                    student.IndexGroup = index;
                    i = 0;
                }
            }

            Console.Write("Enter faculty: ");
            str = Console.ReadLine();
            student.Faculty = str;

            Console.Write("Enter specialization: ");
            str = Console.ReadLine();
            student.Specialization = str;

            for (int i = 4; i > 0; i--)
            {
                Console.Write("Enter average rating: ");
                num = Convert.ToInt32(Console.ReadLine());
                if (num > 0 && num <= 100)
                {
                    student.AverageRating = num;
                    i = 0;
                }
                else
                {
                    Console.Write("Invalid rating. You have " + (i - 1) + " attempts");
                    if (i == 1)
                    {
                        return;
                    }
                }
            }
            students.add(student);
        }

        void showAll()
        {
            Console.WriteLine("-----------------------Students-----------------------");
            Console.WriteLine("");
            var strs = students.printAll();
            string[] infoStudent;
            foreach (string str in strs)
            {
                infoStudent = str.Split(new char[] { '|' });

                Console.WriteLine("Name: " + infoStudent[0] + ' ' + infoStudent[1]);
                Console.WriteLine("Birthday: " + infoStudent[2]);
                Console.WriteLine("Enter date: " + infoStudent[3]);
                Console.WriteLine("Inedx of group: " + infoStudent[4]);
                Console.WriteLine("Faculty: " + infoStudent[5]);
                Console.WriteLine("Specialization: " + infoStudent[6]);
                Console.WriteLine("Average rating: " + infoStudent[7]);
                Console.WriteLine("");
            }

            Console.WriteLine("------------------------------------------------------");
        }
	}
}

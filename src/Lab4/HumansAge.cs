﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp2
{
    public class HumansAge
    {
        public int Year { get; set; }
        public int Month { get; set; }
        public int Day { get; set; }

        public HumansAge()
        {

        }
    }
}

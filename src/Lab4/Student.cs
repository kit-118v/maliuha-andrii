﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp2
{
    public class Student
    {
        public string LastName { get; set; }
        public string Initials { get; set; }
        public DateTime Birthday { get; set; }
        public DateTime EnterDate { get; set; }
        public char IndexGroup { get; set; }
        public string Faculty { get; set; }
        public string Specialization { get; set; }
        public int AverageRating { get; set; }
        public HumansAge StudentsAge { get; }

        public override string ToString()
        {
            return LastName + '|' + Initials + '|' + Birthday.ToString() + '|' + EnterDate.ToString() +
                '|' + IndexGroup + '|' + Faculty + '|' + Specialization + '|' + AverageRating;
        }
        public Student()
        {
            
        }
        public Student(string LastName, string Initials, DateTime Birthday, DateTime EnterDate, char IndexGroup, string Faculty, string Specialization, int AverageRating)
        {
            this.LastName = LastName;
            this.Initials = Initials;
            this.Birthday = Birthday;
            this.EnterDate = EnterDate;
            this.IndexGroup = IndexGroup;
            this.Faculty = Faculty;
            this.Specialization = Specialization;
            this.AverageRating = AverageRating; 
            if (Birthday != null)
            {
                StudentsAge = new HumansAge();
                var date = DateTime.Now;
                StudentsAge.Year = date.Year - Birthday.Year;
                if (date.Month >= Birthday.Month && date.Day >= Birthday.Day)
                {
                    StudentsAge.Month = date.Month - Birthday.Month;
                    StudentsAge.Day = date.Day - Birthday.Day;
                } 
                else
                {
                    StudentsAge.Year -= 1;
                    StudentsAge.Month = date.Month + 11 - Birthday.Month;
                    StudentsAge.Day = DateTime.DaysInMonth(Birthday.Year, Birthday.Month) - Birthday.Day + date.Day;
                    if (StudentsAge.Day > DateTime.DaysInMonth(date.Year, date.Month))
                    {
                        StudentsAge.Month++;
                        StudentsAge.Day -= DateTime.DaysInMonth(date.Year, date.Month);
                    }
                }

            }
            
        }
    }
}
